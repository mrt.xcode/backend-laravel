<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ChecklistController;
use App\Http\Controllers\ChecklistItemController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('login', [AuthController::class, 'login']);
Route::post('register', [AuthController::class, 'register']);

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::get('checklist/{checklistId}/item', [ChecklistItemController::class, 'show']);
    Route::post('checklist/{checklistId}/item', [ChecklistItemController::class, 'store']);
    Route::put('checklist/{checklistId}/item/rename/{checklistItemId}', [ChecklistItemController::class, 'update']);
    Route::get('checklist/{checklistId}/item/{checklistItemId}', [ChecklistItemController::class, 'detail']);
    Route::put('checklist/{checklistId}/item/{checklistItemId}', [ChecklistItemController::class, 'updateStatus']);
    Route::delete('checklist/{checklistId}/item/{checklistItemId}', [ChecklistItemController::class, 'destroy']);
    Route::get('checklist', [ChecklistController::class, 'index']);
    Route::post('checklist', [ChecklistController::class, 'store']);
    Route::delete('checklist/{checklistId}', [ChecklistController::class, 'destroy']);
});
