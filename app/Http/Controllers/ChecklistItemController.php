<?php

namespace App\Http\Controllers;

use App\Models\Checklist;
use App\Models\ChecklistItem;
use Illuminate\Http\Request;

class ChecklistItemController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Checklist $checklistId, Request $request)
    {
        // return $checklistId->id;
        $request->validate([
            'itemName' => 'required'
        ]);
        ChecklistItem::create(['itemName' => $request->itemName, 'checklist_id' => $checklistId->id]);
        return response()->json(['message' => 'success']);
    }

    /**
     * Display the specified resource.
     */
    public function show(Checklist $checklistId)
    {
        $data = ChecklistItem::where('checklist_id', $checklistId->id)->get();
        $result = [];
        foreach ($data as $key => $value) {
            $result[] = [
                'itemName' => $value->itemName
            ];
        }
        return response()->json($result);
    }

    /**
     * Update the specified resource in storage.
     */

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Checklist $checklistId, ChecklistItem $checklistItemId)
    {
        ChecklistItem::where('checklist_id', $checklistId->id)->where('id', $checklistItemId->id)->delete();
        return response()->json(['message' => 'Data has been deleted...']);
    }
    public function detail(Checklist $checklistId, ChecklistItem $checklistItemId)
    {
        $data = ChecklistItem::where('checklist_id', $checklistId->id)->where('id', $checklistItemId->id)->first();
        return response()->json($data);
    }
    public function updateStatus(Checklist $checklistId, ChecklistItem $checklistItemId)
    {
        ChecklistItem::where('checklist_id', $checklistId->id)->where('id', $checklistItemId->id)->update(['active' => !$checklistItemId->active]);
        return response()->json(['message' => 'status has been update...']);
    }
    public function update(Checklist $checklistId, ChecklistItem $checklistItemId)
    {
        $itemName =  json_decode(file_get_contents('php://input'))->itemName;
        ChecklistItem::where('checklist_id', $checklistId->id)->where('id', $checklistItemId->id)->update(['itemName' => $itemName]);
        return response()->json(['message' => 'data has been update...']);
    }
}
